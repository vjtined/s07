//First function: oddEvenChecker
function oddEvenChecker(num1){
	if(typeof num1 === 'number'){
		console.log("The input is a valid number let me check if it is Odd or Even.");
		numCheck = num1%2
		if (numCheck == 0){
			console.log("The number is even.");
		} else {
			console.log("The number is odd.");
		}
	} else {
		alert("Invalid input.")
	};
};

oddEvenChecker(19);

//Second Function: budgetChecker()
function budgetChecker(budget){
	if(typeof budget === 'number'){
		console.log("The input is a valid number let me check if it is over or less than the recommended budget.");
		
		if (budget > 40000){
			console.log("You are over the budget.");
		} else {
			console.log("You have resources left.");
		}
	} else {
		alert("Invalid input.")
	};
};

budgetChecker(36875);









